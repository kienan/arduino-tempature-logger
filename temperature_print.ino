#include "OneWire.h"

OneWire  ds(7);  // on pin 7

void setup(void) {
  Serial.begin(9600);
}

void loop(void) {
  byte i, temp, temp_fraction;
  byte present = 0;
  byte data[12];
  byte addr[8];
  double realtemp = 0.0f;
  
  if ( !ds.search(addr)) {
    Serial.print("No more addresses.\n");
    ds.reset_search();
    delay(250);
    return;
  }
  
  Serial.print("R=");
  for( i = 0; i < 8; i++) {
    Serial.print(addr[i], HEX);
    Serial.print(" ");
  }

  if ( OneWire::crc8( addr, 7) != addr[7]) {
      Serial.print("CRC is not valid!\n");
      return;
  }
  
  if ( addr[0] != 0x28) {
      Serial.print("Device is not a DS18B20 family device.\n");
      return;
  }

  // The DallasTemperature library can do all this work for you!

  ds.reset();
  ds.select(addr);
  ds.write(0x44,1);         // start conversion, with parasite power on at the end
  
  delay(1000);     // maybe 750ms is enough, maybe not
  // we might do a ds.depower() here, but the reset will take care of it.
  
  present = ds.reset();
  ds.select(addr);    
  ds.write(0xBE);         // Read Scratchpad

  Serial.print("P=");
  Serial.print(present,HEX);
  Serial.print(" ");
  for ( i = 0; i < 9; i++) {           // we need 9 bytes
    data[i] = ds.read();
    Serial.print(data[i], HEX);
    Serial.print(" ");
  }
  
  if (data[1]== 0b11111000) {
    Serial.print("-"); 
  }
  else {
    Serial.print("+");
  }
  
  temp = (data[1] & 0b00000111) << 4;
  temp = ((data[0] & 0b11110000) >> 4) | temp;
  realtemp = (double)temp + ((double) (data[0] & 0b00001111) * 0.0625);
  Serial.print(realtemp); 
  Serial.print(" CRC=");
  Serial.print( OneWire::crc8( data, 8), HEX);
  Serial.println();
}

